import styles from "@/app/page.module.css";

import { TeamPlaque } from "@/components/TeamPlaque";

interface DraggableItemProps {
  team: any;
  index: number;
}

const DraggableItem = ({ team, index }: DraggableItemProps) => (
    <div className={styles.draggable}>
      <TeamPlaque team={team} position={index + 1} />
    </div>
  );

export default DraggableItem;
