"use client";
import { ReactSortable } from "react-sortablejs";

import styles from "@/app/page.module.css";

import DraggableItem from "@/app/DraggableItem";
import { NBATeam } from "@/app/Teams";

export interface PredictionDragAndDropProps {
  teams: Array<NBATeam & { id: number }>;
  setTeams: any;
  boxesPerRow: number;
}

const PredictionDragAndDrop = ({
  setTeams,
  teams,
  boxesPerRow,
}: PredictionDragAndDropProps) => (
    <ReactSortable
      list={teams}
      setList={setTeams}
      className={styles.predictionWrapper}
      style={{
        display: "grid",
        gridTemplateColumns: `repeat(${boxesPerRow},1fr)`,
        gridTemplateRows: `repeat(${Math.round(teams.length / boxesPerRow)}, 50px`,
        height: `${Math.round(teams.length / boxesPerRow) * 50}px`,
      }}
    >
      {teams.map((team, index) => (
        <DraggableItem team={team} index={index} key={team.name} />
      ))}
    </ReactSortable>
  );

export default PredictionDragAndDrop;
