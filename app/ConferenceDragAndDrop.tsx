import { useEffect, useState } from "react";
import { Button } from "@mui/material";

import PredictionDragAndDrop, {
  PredictionDragAndDropProps,
} from "@/app/PredictionDragAndDrop";
import { snakeConcatenate } from "@/utils/snakeConcatenate";

const ConferenceDragAndDrop = ({
  teams,
  setTeams,
  boxesPerRow,
}: PredictionDragAndDropProps) => {
  const [easternTeams, setEasternTeams] = useState(
    teams
      .filter((team) => team.conference === "Eastern")
      .map((team, index) => ({ ...team, id: index }))
  );
  const [westernTeams, setWesternTeams] = useState(
    teams
      .filter((team) => team.conference === "Western")
      .map((team, index) => ({ ...team, id: index }))
  );
  const divisionPages = [
    { setTeams: setEasternTeams, teams: easternTeams },
    {
      setTeams: setWesternTeams,
      teams: westernTeams,
    },
  ];
  useEffect(() => {
    setTeams(snakeConcatenate(divisionPages.map(({ teams }) => teams)));
  }, [easternTeams, westernTeams]);
  const [page, setPage] = useState(true);

  return (
    <div
      style={{
        alignItems: "center",
        display: "flex",
        justifyContent: "space-between",
        maxWidth: "min(90vw, 800px)",
      }}
    >
      <Button onClick={() => setPage((prev) => !prev)}>{"<"}</Button>
      <PredictionDragAndDrop
        teams={divisionPages[Number(page)].teams}
        setTeams={divisionPages[Number(page)].setTeams}
        boxesPerRow={boxesPerRow > 2 ? 3 : 1}
      />
      <Button onClick={() => setPage((prev) => !prev)}>{">"}</Button>
    </div>
  );
};

export default ConferenceDragAndDrop;
