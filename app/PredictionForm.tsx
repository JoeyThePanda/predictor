"use client";
import React, { FormEvent, useEffect, useState } from "react";
import { FormControl, FormLabel, Input } from "@mui/joy";
import Option from "@mui/joy/Option";
import Select from "@mui/joy/Select";
import { Button, ButtonGroup } from "@mui/material";
import { createClientComponentClient } from "@supabase/auth-helpers-nextjs";
import { useRouter } from "next/navigation";

import styles from "@/app/page.module.css";

import ConferenceDragAndDrop from "@/app/ConferenceDragAndDrop";
import DivisionDragAndDrop from "@/app/DivisionDragAndDrop";
import PredictionDragAndDrop from "@/app/PredictionDragAndDrop";
import { NBA_TEAMS_MAP, NBATeam } from "@/app/Teams";
import { Database } from "@/types/supabase";
import { useWindowSize } from "@/utils/useWindowSize";

const PredictionForm = () => {
  const [leagueTeams, setLeagueTeams] = useState<
    Array<NBATeam & { id: number }>
  >(NBA_TEAMS_MAP.map((team, index) => ({ ...team, id: index })));

  const [mode, setMode] = useState<"league" | "conference" | "division">(
    "league"
  );
  const [identifier, setIdentifier] = useState<string>("");
  const [name, setName] = useState<string>("");
  const [favoriteTeam, setFavoriteTeam] = useState<number>(30);
  const supabase = createClientComponentClient<Database>();
  const router = useRouter();
  const [boxesPerRow, setBoxesPerRow] = useState(6);
  const { windowWidth } = useWindowSize();

  useEffect(() => {
    const handleWindowResize = () => {
      setBoxesPerRow(Math.min(Math.round((windowWidth * 0.8) / 250), 6));
    };
    handleWindowResize();
  }, [windowWidth]);

  const onSubmit = async (e: FormEvent) => {
    e.preventDefault();
    if (!name || !identifier) return alert("Information Missing");
    const indexList = leagueTeams.map((prediction) =>
      NBA_TEAMS_MAP.findIndex((team) => team.name === prediction.name)
    );
    const { data: previousPredictions } = await supabase
      .from("predictor_predictions")
      .select("*")
      .eq("name", name)
      .eq("identifier", identifier);
    const today = new Date().toISOString().slice(0, 15);
    const todaysPrediction = previousPredictions?.find((prediction) => {
      if (!prediction.created_at) return false;
      const date = new Date(prediction.created_at).toISOString().slice(0, 15);
      return date === today;
    });
    if (todaysPrediction)
      return alert("You already predicted today. Come back tomorrow.");
    const { error } = await supabase.from("predictor_predictions").insert({
      favorite_team: favoriteTeam,
      identifier,
      mode,
      name,
      prediction: indexList.join(":"),
    });
    if (error) return alert(error.message);
    router.push("/leaderboards");
  };

  return (
    <>
      <ButtonGroup>
        <Button
          type={"button"}
          variant={"contained"}
          onClick={() => setMode("league")}
        >
          League
        </Button>
        <Button
          type={"button"}
          variant={"contained"}
          onClick={() => setMode("conference")}
        >
          Conference
        </Button>
        <Button
          type={"button"}
          variant={"contained"}
          onClick={() => setMode("division")}
        >
          Division
        </Button>
      </ButtonGroup>

      {mode === "league" && (
        <PredictionDragAndDrop
          teams={leagueTeams}
          setTeams={setLeagueTeams}
          boxesPerRow={boxesPerRow}
        />
      )}
      {mode === "conference" && (
        <ConferenceDragAndDrop
          teams={leagueTeams}
          setTeams={setLeagueTeams}
          boxesPerRow={boxesPerRow}
        />
      )}
      {mode === "division" && (
        <DivisionDragAndDrop
          teams={leagueTeams}
          setTeams={setLeagueTeams}
          boxesPerRow={boxesPerRow}
        />
      )}
      <form className={styles.submitForm} onSubmit={onSubmit}>
        <FormControl>
          <FormLabel>Display Name</FormLabel>
          <Input
            value={name}
            placeholder={"Lebron"}
            onChange={(e) => setName(e.target.value)}
          />
        </FormControl>

        <FormControl>
          <FormLabel>Secret Identifier (NOT A PASSWORD)</FormLabel>
          <Input
            id={"identifier"}
            value={identifier}
            placeholder={"JustAKidFromAkron"}
            onChange={(e) => setIdentifier(e.target.value)}
          />
        </FormControl>
        <FormControl>
          <FormLabel htmlFor="select-field-demo-button">
            Favorite team
          </FormLabel>
          <Select
            id={"select-field-demo-button"}
            defaultValue={30}
            onChange={(_, newValue) => setFavoriteTeam(newValue ?? 30)}
          >
            <Option value={30}>Refs</Option>
            {NBA_TEAMS_MAP.map((team, index) => (
              <Option key={team.name} value={index}>
                {team.name}
              </Option>
            ))}
          </Select>
        </FormControl>
        <Button type={"submit"} variant={"contained"}>Submit</Button>
      </form>
    </>
  );
};

export default PredictionForm;
