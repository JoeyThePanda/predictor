import { StaticImageData } from "next/image";

import JazzJersey from "@/public/jerseys/JazzJersey.png";
import KingsJersey from "@/public/jerseys/KingsJersey.png";
import RaptorsJersey from "@/public/jerseys/RaptorsJersey.png";
import ThunderJersey from "@/public/jerseys/ThunderJersey.png";
import HawksLogo from "@/public/logos/atlanta-hawks-logo.svg";
import CelticsLogo from "@/public/logos/boston-celtics-logo.svg";
import NetsLogo from "@/public/logos/brooklyn-nets-logo.svg";
import HornetsLogo from "@/public/logos/charlotte-hornets-logo.svg";
import BullsLogo from "@/public/logos/chicago-bulls-logo.svg";
import CavsLogo from "@/public/logos/cleveland-cavaliers-logo.svg";
import MavericksLogo from "@/public/logos/dallas-mavericks-logo.svg";
import NuggetsLogo from "@/public/logos/denver-nuggets-logo.svg";
import PistonsLogo from "@/public/logos/detroit-pistons-logo.svg";
import WarriorsLogo from "@/public/logos/golden-state-warriors-logo.svg";
import RocketsLogo from "@/public/logos/houston-rockets-logo.svg";
import PacersLogo from "@/public/logos/indiana-pacers-logo.svg";
import ClippersLogo from "@/public/logos/los-angeles-clippers-logo.svg";
import LakersLogo from "@/public/logos/los-angeles-lakers-logo.svg";
import GrizzliesLogo from "@/public/logos/memphis-grizzlies-logo.svg";
import HeatLogo from "@/public/logos/miami-heat-logo.svg";
import BucksLogo from "@/public/logos/milwaukee-bucks-logo.svg";
import TimberwolvesLogo from "@/public/logos/minnesota-timberwolves-logo.svg";
import PelicansLogo from "@/public/logos/new-orleans-pelicans-logo.svg";
import KnicksLogo from "@/public/logos/new-york-knicks-logo.svg";
import ThunderLogo from "@/public/logos/oklahoma-city-thunder-logo.svg";
import MagicLogo from "@/public/logos/orlando-magic-logo.svg";
import SixersLogo from "@/public/logos/philadelphia-76ers-logo.svg";
import SunsLogo from "@/public/logos/phoenix-suns-logo.svg";
import TrailblazersLogo from "@/public/logos/portland-trailblazers-logo.svg";
import KingsLogo from "@/public/logos/sacramento-kings-logo.svg";
import SpursLogo from "@/public/logos/san-antonio-spurs-logo.svg";
import RaptorsLogo from "@/public/logos/toronto-raptors-logo.svg";
import JazzLogo from "@/public/logos/utah-jazz-logo.svg";
import WizardsLogo from "@/public/logos/washington-wizards-logo.svg";

export enum Conference {
  Eastern = "Eastern",
  Western = "Western",
}

export enum Division {
  Pacific = "Pacific",
  NorthWest = "NorthWest",
  SouthWest = "SouthWest",
  Atlantic = "Atlantic",
  Central = "Central",
  SouthEast = "SouthEast",
}

export interface NBATeam {
  city: string;
  name: string;
  shortName: string;
  logo: StaticImageData;
  jersey?: StaticImageData;
  colors: {
    primary: string;
    text: string;
    secondary: string;
    secondaryText: string;
  };
  conference: Conference;
  division: Division;
}

export const NBA_TEAMS_MAP: Array<NBATeam> = [
  {
    city: "Philadelphia",
    colors: {
      primary: "#ed174c",
      secondary: "#006bb6",
      secondaryText: "white",
      text: "white",
    },
    conference: Conference.Eastern,
    division: Division.Atlantic,
    logo: SixersLogo,
    name: "76ers",
    shortName: "PHI",
  },
  {
    city: "Milwaukee",
    colors: {
      primary: "#00471B",
      secondary: "#EEE1C6",
      secondaryText: "black",
      text: "white",
    },
    conference: Conference.Eastern,
    division: Division.Central,
    logo: BucksLogo,
    name: "Bucks",
    shortName: "MIL",
  },
  {
    city: "Chicago",
    colors: {
      primary: "#CE1141",
      secondary: "#000000",
      secondaryText: "white",
      text: "white",
    },
    conference: Conference.Eastern,
    division: Division.Central,
    logo: BullsLogo,
    name: "Bulls",
    shortName: "CHI",
  },
  {
    city: "Cleveland",
    colors: {
      primary: "#ffb81c",
      secondary: "#6f263d",
      secondaryText: "white",
      text: "white",
    },
    conference: Conference.Eastern,
    division: Division.Central,
    logo: CavsLogo,
    name: "Cavaliers",
    shortName: "CLE",
  },
  {
    city: "Boston",
    colors: {
      primary: "#FFFFFF",
      secondary: "#008348",
      secondaryText: "white",
      text: "white",
    },
    conference: Conference.Eastern,
    division: Division.Atlantic,
    logo: CelticsLogo,
    name: "Celtics",
    shortName: "BOS",
  },
  {
    city: "Los Angeles",
    colors: {
      primary: "#c8102e",
      secondary: "#1d428a",
      secondaryText: "white",
      text: "white",
    },
    conference: Conference.Western,
    division: Division.Pacific,
    logo: ClippersLogo,
    name: "Clippers",
    shortName: "LAC",
  },
  {
    city: "Memphis",
    colors: {
      primary: "#12173F",
      secondary: "#5D76A9",
      secondaryText: "white",
      text: "white",
    },
    conference: Conference.Western,
    division: Division.SouthWest,
    logo: GrizzliesLogo,
    name: "Grizzlies",
    shortName: "MEM",
  },
  {
    city: "Atlanta",
    colors: {
      primary: "#e03a3e",
      secondary: "#26282a",
      secondaryText: "white",
      text: "white",
    },
    conference: Conference.Eastern,
    division: Division.SouthEast,
    logo: HawksLogo,
    name: "Hawks",
    shortName: "ATL",
  },
  {
    city: "Miami",
    colors: {
      primary: "#98002e",
      secondary: "#f9a01b",
      secondaryText: "black",
      text: "white",
    },
    conference: Conference.Eastern,
    division: Division.SouthEast,
    logo: HeatLogo,
    name: "Heat",
    shortName: "MIA",
  },
  {
    city: "Charlotte",
    colors: {
      primary: "#1d1160",
      secondary: "#00788C",
      secondaryText: "white",
      text: "white",
    },
    conference: Conference.Eastern,
    division: Division.SouthEast,
    logo: HornetsLogo,
    name: "Hornets",
    shortName: "CHA",
  },
  {
    city: "Utah",
    colors: {
      primary: "#fff31d",
      secondary: "black",
      secondaryText: "white",
      text: "black",
    },
    conference: Conference.Western,
    division: Division.NorthWest,
    jersey: JazzJersey,
    logo: JazzLogo,
    name: "Jazz",
    shortName: "UTA",
  },
  {
    city: "Sacramento",
    colors: {
      primary: "#63727a",
      secondary: "#5a2b81",
      secondaryText: "white",
      text: "white",
    },
    conference: Conference.Western,
    division: Division.Pacific,
    jersey: KingsJersey,
    logo: KingsLogo,
    name: "Kings",
    shortName: "SAC",
  },
  {
    city: "New York",
    colors: {
      primary: "#f58426",
      secondary: "#006BB6",
      secondaryText: "white",
      text: "black",
    },
    conference: Conference.Eastern,
    division: Division.Atlantic,
    logo: KnicksLogo,
    name: "Knicks",
    shortName: "NYK",
  },
  {
    city: "Los Angeles",
    colors: {
      primary: "#FDB927",
      secondary: "#552583",
      secondaryText: "white",
      text: "black",
    },
    conference: Conference.Western,
    division: Division.Pacific,
    logo: LakersLogo,
    name: "Lakers",
    shortName: "LAL",
  },
  {
    city: "Orlando",
    colors: {
      primary: "#0077c0",
      secondary: "#c4ced4",
      secondaryText: "black",
      text: "white",
    },
    conference: Conference.Eastern,
    division: Division.SouthEast,
    logo: MagicLogo,
    name: "Magic",
    shortName: "ORL",
  },
  {
    city: "Dallas",
    colors: {
      primary: "#00538C",
      secondary: "#002B5e",
      secondaryText: "white",
      text: "white",
    },
    conference: Conference.Western,
    division: Division.SouthWest,
    logo: MavericksLogo,
    name: "Mavericks",
    shortName: "DAL",
  },
  {
    city: "Brooklyn",
    colors: {
      primary: "#FFFFFF",
      secondary: "#000000",
      secondaryText: "white",
      text: "black",
    },
    conference: Conference.Eastern,
    division: Division.Atlantic,
    logo: NetsLogo,
    name: "Nets",
    shortName: "BKN",
  },
  {
    city: "Denver",
    colors: {
      primary: "#fec524",
      secondary: "#0e2240",
      secondaryText: "white",
      text: "white",
    },
    conference: Conference.Western,
    division: Division.NorthWest,
    logo: NuggetsLogo,
    name: "Nuggets",
    shortName: "DEN",
  },
  {
    city: "Indiana",
    colors: {
      primary: "#002D62",
      secondary: "#FDBB30",
      secondaryText: "black",
      text: "white",
    },
    conference: Conference.Eastern,
    division: Division.Central,
    logo: PacersLogo,
    name: "Pacers",
    shortName: "IND",
  },
  {
    city: "New Orleans",
    colors: {
      primary: "#0C2340",
      secondary: "#C8102E",
      secondaryText: "white",
      text: "white",
    },
    conference: Conference.Western,
    division: Division.SouthWest,
    logo: PelicansLogo,
    name: "Pelicans",
    shortName: "NOP",
  },
  {
    city: "Detroit",
    colors: {
      primary: "#C8102E",
      secondary: "#1d42ba",
      secondaryText: "white",
      text: "white",
    },
    conference: Conference.Eastern,
    division: Division.Central,
    logo: PistonsLogo,
    name: "Pistons",
    shortName: "DET",
  },
  {
    city: "Toronto",
    colors: {
      primary: "#ce1141",
      secondary: "#000000",
      secondaryText: "white",
      text: "white",
    },
    conference: Conference.Eastern,
    division: Division.Atlantic,
    jersey: RaptorsJersey,
    logo: RaptorsLogo,
    name: "Raptors",
    shortName: "TOR",
  },
  {
    city: "Houston",
    colors: {
      primary: "#ce1141",
      secondary: "#c4ced4",
      secondaryText: "black",
      text: "white",
    },
    conference: Conference.Western,
    division: Division.SouthWest,
    logo: RocketsLogo,
    name: "Rockets",
    shortName: "HOU",
  },
  {
    city: "San Antonio",
    colors: {
      primary: "#000000",
      secondary: "#c4ced4",
      secondaryText: "black",
      text: "white",
    },
    conference: Conference.Western,
    division: Division.SouthWest,
    logo: SpursLogo,
    name: "Spurs",
    shortName: "SAS",
  },
  {
    city: "Phoenix",
    colors: {
      primary: "#e56020",
      secondary: "#1d1160",
      secondaryText: "white",
      text: "white",
    },
    conference: Conference.Western,
    division: Division.Pacific,
    logo: SunsLogo,
    name: "Suns",
    shortName: "PHO",
  },
  {
    city: "Oklahoma City",
    colors: {
      primary: "#ef3b24",
      secondary: "#007ac1",
      secondaryText: "white",
      text: "white",
    },
    conference: Conference.Western,
    division: Division.NorthWest,
    jersey: ThunderJersey,
    logo: ThunderLogo,
    name: "Thunder",
    shortName: "OKC",
  },
  {
    city: "Minnesota",
    colors: {
      primary: "#78be20",
      secondary: "#0c2340",
      secondaryText: "white",
      text: "white",
    },
    conference: Conference.Western,
    division: Division.NorthWest,
    logo: TimberwolvesLogo,
    name: "Timberwolves",
    shortName: "MIN",
  },
  {
    city: "Portland",
    colors: {
      primary: "#E03A3E",
      secondary: "#000000",
      secondaryText: "white",
      text: "white",
    },
    conference: Conference.Western,
    division: Division.NorthWest,
    logo: TrailblazersLogo,
    name: "Trailblazers",
    shortName: "POR",
  },
  {
    city: "Golden State",
    colors: {
      primary: "#fdb927",
      secondary: "#006bb6",
      secondaryText: "white",
      text: "white",
    },
    conference: Conference.Western,
    division: Division.Pacific,
    logo: WarriorsLogo,
    name: "Warriors",
    shortName: "GSW",
  },
  {
    city: "Washington",
    colors: {
      primary: "#e31837",
      secondary: "#002b5c",
      secondaryText: "white",
      text: "white",
    },
    conference: Conference.Eastern,
    division: Division.SouthEast,
    logo: WizardsLogo,
    name: "Wizards",
    shortName: "WAS",
  },
];
