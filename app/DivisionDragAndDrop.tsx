"use client";
import { useEffect, useState } from "react";
import { Button } from "@mui/material";

import PredictionDragAndDrop, {
  PredictionDragAndDropProps,
} from "@/app/PredictionDragAndDrop";
import { snakeConcatenate } from "@/utils/snakeConcatenate";

const DivisionDragAndDrop = ({
  teams,
  setTeams,
}: PredictionDragAndDropProps) => {
  const [southEastTeams, setSouthEastTeams] = useState(
    teams
      .filter((team) => team.division === "SouthEast")
      .map((team, index) => ({ ...team, id: index }))
  );
  const [southWestTeams, setSouthWestTeams] = useState(
    teams
      .filter((team) => team.division === "SouthWest")
      .map((team, index) => ({ ...team, id: index }))
  );

  const [pacificTeams, setPacificTeams] = useState(
    teams
      .filter((team) => team.division === "Pacific")
      .map((team, index) => ({ ...team, id: index }))
  );
  const [atlanticTeams, setAtlanticTeams] = useState(
    teams
      .filter((team) => team.division === "Atlantic")
      .map((team, index) => ({ ...team, id: index }))
  );

  const [centralTeams, setCentralTeams] = useState(
    teams
      .filter((team) => team.division === "Central")
      .map((team, index) => ({ ...team, id: index }))
  );

  const [northWestTeams, setNorthWestTeams] = useState(
    teams
      .filter((team) => team.division === "NorthWest")
      .map((team, index) => ({ ...team, id: index }))
  );

  const divisionPages = [
    { setTeams: setAtlanticTeams, teams: atlanticTeams },
    {
      setTeams: setCentralTeams,
      teams: centralTeams,
    },
    { setTeams: setPacificTeams, teams: pacificTeams },
    { setTeams: setNorthWestTeams, teams: northWestTeams },
    { setTeams: setSouthWestTeams, teams: southWestTeams },
    { setTeams: setSouthEastTeams, teams: southEastTeams },
  ];
  useEffect(() => {
    setTeams(snakeConcatenate(divisionPages.map(({ teams }) => teams)));
  }, [
    atlanticTeams,
    pacificTeams,
    centralTeams,
    northWestTeams,
    southEastTeams,
    southWestTeams,
  ]);
  const [page, setPage] = useState(0);
  return (
    <div
      style={{
        alignItems: "center",
        display: "flex",
        justifyContent: "space-between",
        maxWidth: "min(90vw, 400px)",
      }}
    >
      <Button
        onClick={() => setPage((prev) => Math.max(0, prev - 1))}
        style={{ color: page > 0 ? "inherit" : "grey" }}
      >
        {"<"}
      </Button>
      <PredictionDragAndDrop
        teams={divisionPages[page].teams}
        setTeams={divisionPages[page].setTeams}
        boxesPerRow={1}
      />
      <Button
        onClick={() => setPage((prev) => Math.min(5, prev + 1))}
        style={{ color: page < 5 ? "inherit" : "grey" }}
      >
        {">"}
      </Button>
    </div>
  );
};

export default DivisionDragAndDrop;
