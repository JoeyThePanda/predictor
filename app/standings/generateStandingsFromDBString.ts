import { NBA_TEAMS_MAP } from "@/app/Teams";

export const generateStandingsFromDBString = (dbString: string) => {
  const standingsArray = dbString.split(":");
  return standingsArray.map((team) => NBA_TEAMS_MAP[Number(team)]);
};
