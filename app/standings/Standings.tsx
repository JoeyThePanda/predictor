import React from "react";
import { clsx } from "clsx";

import styles from "./page.module.css";

import { Conference, Division, NBATeam } from "@/app/Teams";
import { TeamPlaque } from "@/components/TeamPlaque";

const Standings = ({
  standings,
  mode,
}: {
  standings: Array<NBATeam>;
  mode: "league" | "conference" | "division";
}) => {
  const teamsByDivision: Record<Division, NBATeam[]> = {
    Atlantic: standings.filter((team) => team.division === "Atlantic"),
    Central: standings.filter((team) => team.division === "Central"),
    NorthWest: standings.filter((team) => team.division === "NorthWest"),
    Pacific: standings.filter((team) => team.division === "Pacific"),
    SouthEast: standings.filter((team) => team.division === "SouthEast"),
    SouthWest: standings.filter((team) => team.division === "SouthWest"),
  };
  const teamsByConference: Record<Conference, NBATeam[]> = {
    Eastern: standings.filter((team) => team.conference === "Eastern"),
    Western: standings.filter((team) => team.conference === "Western"),
  };

  return (
    <div
      className={clsx(
        styles.standings,
        mode == "league" && styles.leagueStandings
      )}
    >
      {mode === "league" ? (
        <div style={{ justifySelf: "center" }}>
          {standings.map((team, index) => (
            <TeamPlaque team={team} position={index + 1} key={team.name} />
          ))}
        </div>
      ) : mode === "conference" ? (
        Object.values(Conference).map((conference) => (
          <div key={conference}>
            <h3>{conference}</h3>
            <div>
              {teamsByConference[conference].map((team, index) => (
                <TeamPlaque team={team} position={index + 1} key={team.name} />
              ))}
            </div>
          </div>
        ))
      ) : (
        Object.values(Division).map((division) => (
          <div key={division}>
            <h3>{division}</h3>
            {teamsByDivision[division].map((team, index) => (
              <>
                <TeamPlaque team={team} position={index + 1} key={team.name} />
              </>
            ))}
          </div>
        ))
      )}
    </div>
  );
};

export default Standings;
