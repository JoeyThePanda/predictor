"use client";
import React, { useEffect, useState } from "react";
import { Button, ButtonGroup } from "@mui/material";
import { DatePicker, LocalizationProvider } from "@mui/x-date-pickers";
import { AdapterDayjs } from "@mui/x-date-pickers/AdapterDayjs";
import dayjs from "dayjs";

import styles from "./page.module.css"

import { generateStandingsFromDBString } from "@/app/standings/generateStandingsFromDBString";
import Standings from "@/app/standings/Standings";
import { Database } from "@/types/supabase";

const LeagueStandings = ({
  allStandings,
}: {
  allStandings: Array<Database["public"]["Tables"]["predictor_actual"]["Row"]>;
}) => {
  const [selectedDate, setSelectedDate] = useState(dayjs());
  const [standings, setStandings] = useState(
    generateStandingsFromDBString(allStandings[allStandings.length - 1].actual)
  );
  useEffect(() => {
    const standingsAtDate = allStandings.filter(
      (obj) => dayjs(obj.created_at ?? "") < selectedDate
    )[0];
    setStandings(
      generateStandingsFromDBString(
        standingsAtDate ? standingsAtDate.actual : allStandings[0].actual
      )
    );
  }, [allStandings, selectedDate]);
  const [mode, setMode] = useState<"league" | "conference" | "division">(
    "league"
  );
  return (
    <div className={styles.content}>
      <LocalizationProvider dateAdapter={AdapterDayjs}>
        <DatePicker
          value={selectedDate}
          onChange={(newDate) => setSelectedDate((prev) => newDate ?? prev)}
        />
      </LocalizationProvider>
      <ButtonGroup>
        <Button
          type={"button"}
          variant={"contained"}
          onClick={() => setMode("league")}
        >
          League
        </Button>
        <Button
          type={"button"}
          variant={"contained"}
          onClick={() => setMode("conference")}
        >
          Conference
        </Button>
        <Button
          type={"button"}
          variant={"contained"}
          onClick={() => setMode("division")}
        >
          Division
        </Button>
      </ButtonGroup>
      <Standings standings={standings} mode={mode} />
    </div>
  );
};

export default LeagueStandings;
