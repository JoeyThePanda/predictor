import {
  createClientComponentClient,
} from "@supabase/auth-helpers-nextjs";

import LeagueStandings from "@/app/standings/LeagueStandings";
import { Database } from "@/types/supabase";

export const revalidate = 3600;

const Page = async () => {
  const supabase = createClientComponentClient<Database>();
  const { data: standings } = await supabase
    .from("predictor_actual")
    .select("*")
    .order("created_at")

  if (!standings) return <p>ERROR</p>;
  return (
    <LeagueStandings allStandings={standings}/>
  );
};

export default Page;
