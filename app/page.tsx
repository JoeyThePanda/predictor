"use client";
import { useState } from "react";
import { Modal, ModalClose } from "@mui/joy";

import PredictionForm from "@/app/PredictionForm";

export default function Home() {
  const [showModal, setShowModal] = useState(false);
  return (
    <>
      <h1>NBA Predictor</h1>
      <p>
        Predict the end of season standings.{" "}
        <span
          style={{
            borderBottom: "solid 1px orange",
            color: "orange",
            cursor: "pointer",
          }}
          onClick={() => setShowModal(true)}
        >
          More Info.
        </span>
      </p>
      <PredictionForm />
      <Modal
        open={showModal}
        onClose={() => setShowModal(false)}
        style={{
          left: "50%",
          position: "absolute",
          top: "50%",
          transform: "translate(-50%, -50%)",
          width: "450px",
        }}
      >
        <div
          style={{
            alignItems: "center",
            background: "white",
            color: "black",
            display: "flex",
            flexDirection: "column",
            fontSize: "0.8rem",
            justifyContent: "space-around",
            maxWidth: "90vw",
            minHeight: "350px",
            padding: "1rem 2rem",
          }}
        >
          <ModalClose />
          <p>
            Sort the teams into the order that you predict they will be at the
            end of the season. You will be awarded points based on how close you
            were and how early you made your prediction.
          </p>
          <p>
            You can make one prediction using the same combination of Display
            name and SecretIdentifier per day. During the season I will release
            some statistics that will be grouped based on these. If you
            don&apos;t care about these, just enter whatever you want. Just note
            that everything is stored in plain text.
          </p>
          <p>Have fun!</p>
        </div>
      </Modal>
    </>
  );
}
