"use client";
import { ReactNode } from "react";
import {
  DataGrid,
  GridColDef,
  GridToolbarContainer,
  GridToolbarQuickFilter,
} from "@mui/x-data-grid";
import Image from "next/image";

import { PredictionWithScore } from "@/app/leaderboards/getLeaderboardDataFromPredictions";
import { NBA_TEAMS_MAP } from "@/app/Teams";
import NBALogo from "@/public/logos/nba-logo.png";

export const LeaderboardBody = ({
  leaderboardData,
}: {
  leaderboardData: PredictionWithScore[];
}) => {
  const columns: GridColDef[] = [
    { field: "Rank", width: 90 },
    {
      field: "Name",
      renderCell: ({ value }) => {
        console.log(value);
        const [name, team] = value.split(";");
        return (
          <div style={{ alignItems: "center", display: "flex" }}>
            <div
              style={{
                height: team ? "40px" : "25px",
                marginRight: "10px",
                position: "relative",
                width: "25px",
              }}
            >
              <Image src={team? JSON.parse(team) : NBALogo} alt={"Logo"} fill />
            </div>
            <p>{name}</p>
          </div>
        );
      },
      sortable: false,
      valueGetter: (params) =>
        `${params.row.Name || ""};${JSON.stringify(params.row.Team) || ""}`,
      width: 200,
    },
    { field: "Score" },
    { field: "Date", type: "date" },
  ];

  const rows = leaderboardData
    ?.sort((a, b) => b.score - a.score)
    .map((prediction, index) => ({
      Date: new Date(prediction.created_at || ""),
      id: index,
      Name: prediction.name,
      Rank: index + 1,
      Score: prediction.score,
      Team: NBA_TEAMS_MAP[prediction.favorite_team]?.logo,
    }));
  return (
    <>
      <DataGrid
        columns={columns}
        rows={rows}
        slots={{
          toolbar: () => (
            <GridToolbarContainer>
              <GridToolbarQuickFilter />
            </GridToolbarContainer>
          ),
        }}
        disableColumnMenu={true}
      />
    </>
  );
};
