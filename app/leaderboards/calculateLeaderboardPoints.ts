const END_OF_APRIL = new Date(2024, 3, 30).valueOf();
export const calculateLeaderboardPoints = (
  predictionDBString: string,
  standingsDBString: string,
  createdAt: string | null
) => {
  const date = new Date(createdAt || END_OF_APRIL).valueOf();
  const bonusPoints =
    Math.round((END_OF_APRIL - date) / (1000 * 60 * 60 * 24)) * 2;
  const predictions = predictionDBString.split(":");
  const standings = standingsDBString.split(":");
  let score = bonusPoints - 500;
  predictions.forEach((prediction, index) => {
    score +=
      30 -
      Math.abs(
        standings.findIndex((standing) => standing === prediction) - index
      );
  });
  return score;
};
