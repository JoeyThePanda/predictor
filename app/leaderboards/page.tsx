import { createClientComponentClient } from "@supabase/auth-helpers-nextjs";

import styles from "./page.module.css";

import { getLeaderboardDataFromPredictions } from "@/app/leaderboards/getLeaderboardDataFromPredictions";
import { LeaderboardBody } from "@/app/leaderboards/LeaderboardBody";
import { Database } from "@/types/supabase";

export const revalidate = 3600;
const page = async () => {
  const supabase = createClientComponentClient<Database>();
  const { data: predictions } = await supabase
    .from("predictor_predictions")
    .select("*")
  const { data: standings } = await supabase
    .from("predictor_actual")
    .select("*")
    .eq("id", 1)
    .single();
  if (!standings) throw new Error();

  const leaderboardData = getLeaderboardDataFromPredictions(
    predictions || [],
    standings
  );
  return (
    <div className={styles.leaderboard}>
      <LeaderboardBody  leaderboardData={leaderboardData}/>

    </div>
  );
};

export default page;
