import { calculateLeaderboardPoints } from "@/app/leaderboards/calculateLeaderboardPoints";
import { Database } from "@/types/supabase";

type Prediction = Database["public"]["Tables"]["predictor_predictions"]["Row"];
export type PredictionWithScore =
  Database["public"]["Tables"]["predictor_predictions"]["Row"] & {
    score: number;
  };
type Standings = Database["public"]["Tables"]["predictor_actual"]["Row"];
export const getLeaderboardDataFromPredictions = (
  predictions: Prediction[],
  standings: Standings
): Array<PredictionWithScore> => {
  const predictionsWithScores = predictions?.map((prediction) => ({
    ...prediction,
    created_at: new Date(prediction.created_at || "").toISOString().slice(0,10),
    score: calculateLeaderboardPoints(
      prediction.prediction,
      standings.actual,
      prediction.created_at
    ),
  }));
  const uniqueUsersMap: Map<string, PredictionWithScore> = new Map();
  predictionsWithScores.forEach((prediction) => {
    const key = `${prediction.name}_${prediction.identifier}`;
    if (
      !uniqueUsersMap.has(key) ||
      prediction.score > (uniqueUsersMap.get(key)?.score || 0)
    ) {
      uniqueUsersMap.set(key, prediction);
    }
  });

  return Array.from(uniqueUsersMap.values());
};
