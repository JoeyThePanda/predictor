import { NBA_TEAMS_MAP } from "@/app/Teams";

export const getDetailedPrediction = (prediction: string) => {
  const predictionArray = prediction.split(":");
  return predictionArray.map(
    (predictionElement) => NBA_TEAMS_MAP[Number(predictionElement)]
  );
};