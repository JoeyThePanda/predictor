"use client";
import { useState } from "react";
import { clsx } from "clsx";
import Image from "next/image";

import styles from "@/app/leaderboards/page.module.css";

import { getDetailedPrediction } from "@/app/leaderboards/getDetailedPrediction";
import { PredictionWithScore } from "@/app/leaderboards/getLeaderboardDataFromPredictions";
import { NBA_TEAMS_MAP } from "@/app/Teams";
import { TeamPlaque } from "@/components/TeamPlaque";
import MonstarsLogo from "@/public/logos/MonstarsLogo.png";

export const LeaderboardRow = ({
  prediction,
  rank,
}: {
  rank: number;
  prediction: PredictionWithScore;
}) => {
  const [showDetails, setShowDetails] = useState(false);
  const detailedPrediction = getDetailedPrediction(prediction.prediction);
  return (
    <>
      <div
        onClick={() => setShowDetails((prev) => !prev)}
        className={clsx(
          styles.leaderboardRow,
          showDetails && styles.leaderboardRowActive
        )}
      >
        <div className={styles.leaderboardRowTop}>
          <div style={{ width: "10%" }}>{rank}</div>
          <div
            style={{
              height: "2.6em",
              margin: "0 5px 0 15px",
              position: "relative",
              width: "1.6em",
            }}
          >
            <Image
              src={
                NBA_TEAMS_MAP[prediction.favorite_team]?.logo ??
                MonstarsLogo
              }
              alt={"Jersey "}
              fill
              sizes={"130px"}
            />
          </div>
          <div style={{ width: "50%" }}>{prediction.name}</div>

          <div>{prediction.score} pts.</div>
        </div>
        {showDetails && (
          <div
            style={{
              display: "flex",
              height: "40px",
              justifyContent: "space-between",
              overflow: "scroll"
            }}
          >
            {detailedPrediction.map((team, index) => (
                <div key={team.name} style={{ margin: "0 12px"  }}>
                  <TeamPlaque team={team} position={index + 1} small />
                </div>
              ))}
          </div>
        )}
      </div>
    </>
  );
};
