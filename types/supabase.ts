export type Json =
  | string
  | number
  | boolean
  | null
  | { [key: string]: Json | undefined }
  | Json[]

export interface Database {
  public: {
    Tables: {
      boards_game_categories: {
        Row: {
          created_at: string | null
          game_id: number
          id: number
          name: string
          restrictions: Json | null
          sort: string
          type: Database["public"]["Enums"]["category_type_enum"]
        }
        Insert: {
          created_at?: string | null
          game_id: number
          id?: number
          name: string
          restrictions?: Json | null
          sort: string
          type?: Database["public"]["Enums"]["category_type_enum"]
        }
        Update: {
          created_at?: string | null
          game_id?: number
          id?: number
          name?: string
          restrictions?: Json | null
          sort?: string
          type?: Database["public"]["Enums"]["category_type_enum"]
        }
        Relationships: [
          {
            foreignKeyName: "boards_game_categories_game_id_fkey"
            columns: ["game_id"]
            referencedRelation: "boards_games"
            referencedColumns: ["id"]
          }
        ]
      }
      boards_game_entries: {
        Row: {
          category_id: number
          created_at: string | null
          id: number
          player_id: number
          session_id: number
          value: string
        }
        Insert: {
          category_id: number
          created_at?: string | null
          id?: number
          player_id: number
          session_id: number
          value: string
        }
        Update: {
          category_id?: number
          created_at?: string | null
          id?: number
          player_id?: number
          session_id?: number
          value?: string
        }
        Relationships: [
          {
            foreignKeyName: "boards_game_entries_category_id_fkey"
            columns: ["category_id"]
            referencedRelation: "boards_game_categories"
            referencedColumns: ["id"]
          },
          {
            foreignKeyName: "boards_game_entries_player_id_fkey"
            columns: ["player_id"]
            referencedRelation: "boards_players"
            referencedColumns: ["id"]
          },
          {
            foreignKeyName: "boards_game_entries_session_id_fkey"
            columns: ["session_id"]
            referencedRelation: "boards_game_sessions"
            referencedColumns: ["id"]
          }
        ]
      }
      boards_game_sessions: {
        Row: {
          created_at: string | null
          game_id: number
          id: number
        }
        Insert: {
          created_at?: string | null
          game_id: number
          id?: number
        }
        Update: {
          created_at?: string | null
          game_id?: number
          id?: number
        }
        Relationships: [
          {
            foreignKeyName: "boards_game_sessions_game_id_fkey"
            columns: ["game_id"]
            referencedRelation: "boards_games"
            referencedColumns: ["id"]
          }
        ]
      }
      boards_games: {
        Row: {
          created_at: string
          id: number
          name: string
          users_id: string
        }
        Insert: {
          created_at?: string
          id?: number
          name?: string
          users_id: string
        }
        Update: {
          created_at?: string
          id?: number
          name?: string
          users_id?: string
        }
        Relationships: [
          {
            foreignKeyName: "boards_games_users_id_fkey"
            columns: ["users_id"]
            referencedRelation: "users"
            referencedColumns: ["id"]
          }
        ]
      }
      boards_players: {
        Row: {
          created_at: string | null
          id: number
          name: string
          user_id: string | null
        }
        Insert: {
          created_at?: string | null
          id?: number
          name: string
          user_id?: string | null
        }
        Update: {
          created_at?: string | null
          id?: number
          name?: string
          user_id?: string | null
        }
        Relationships: [
          {
            foreignKeyName: "boards_players_user_id_fkey"
            columns: ["user_id"]
            referencedRelation: "users"
            referencedColumns: ["id"]
          }
        ]
      }
      predictor_actual: {
        Row: {
          actual: string
          created_at: string | null
          id: number
        }
        Insert: {
          actual: string
          created_at?: string | null
          id?: number
        }
        Update: {
          actual?: string
          created_at?: string | null
          id?: number
        }
        Relationships: []
      }
      predictor_predictions: {
        Row: {
          created_at: string | null
          favorite_team: number
          id: number
          identifier: string
          mode: Database["public"]["Enums"]["prediction_mode_enum"]
          name: string
          prediction: string
        }
        Insert: {
          created_at?: string | null
          favorite_team?: number
          id?: number
          identifier: string
          mode?: Database["public"]["Enums"]["prediction_mode_enum"]
          name: string
          prediction: string
        }
        Update: {
          created_at?: string | null
          favorite_team?: number
          id?: number
          identifier?: string
          mode?: Database["public"]["Enums"]["prediction_mode_enum"]
          name?: string
          prediction?: string
        }
        Relationships: []
      }
    }
    Views: {
      [_ in never]: never
    }
    Functions: {
      [_ in never]: never
    }
    Enums: {
      category_type_enum: "string" | "number" | "choice" | "boolean"
      prediction_mode_enum: "league" | "conference" | "division"
    }
    CompositeTypes: {
      [_ in never]: never
    }
  }
}
