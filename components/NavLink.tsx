"use client";
import { ReactNode, useEffect, useState } from "react";
import { clsx } from "clsx";
import Link from "next/link";
import { usePathname } from "next/navigation";

import styles from "./common.module.css";

const NavLink = ({ href, children }: { href: string; children: ReactNode }) => {
  const [active, setActive] = useState(false);
  const pathName = usePathname();
  useEffect(() => {
    setActive(() => pathName === href);
  }, [href, pathName]);
  return (
    <Link href={href} className={clsx(active && styles.activeLink)}>
      {children}
    </Link>
  );
};

export default NavLink;
