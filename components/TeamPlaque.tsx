import Image from "next/image";

import styles from "./common.module.css";

import { NBATeam } from "@/app/Teams";
import NBALogo from "@/public/logos/nba-logo.png";

export const TeamPlaque = ({
  team,
  position,
  small,
}: {
  team: NBATeam;
  position: number;
  small?: boolean;
}) => (
    <div
      className={styles.teamPlaque}
      style={{
        backgroundColor: team.colors?.secondary ?? "var(--primary)",
        borderColor: team.colors?.primary ?? "black",
        color: team.colors?.secondaryText ?? "var(--primary-text)",
      }}
    >
      <span style={{ width: "0.8em" }}>{position}</span>
      <div
        style={{
          height: small ? "1em" : "1.5em",
          margin: "0 5px 0 15px",
          position: "relative",
          width: small ? "1em" : "1.5em",
        }}
      >
        <Image src={team.logo ?? NBALogo} alt={"detroit"} fill={true} />
      </div>
      <span style={{ flexGrow: 0 }}>{small ? team.shortName : team.name}</span>
    </div>
  );
