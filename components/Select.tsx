import React, { ChangeEventHandler } from "react";

import styles from "./common.module.css";

interface SelectProps {
  onChange: ChangeEventHandler<HTMLSelectElement>;
  value: string | number | readonly string[] | undefined;
  children: React.ReactNode;
  placeholder: string
}

export const Select = ({ onChange, value, children, placeholder }: SelectProps) => (
    <select value={value} onChange={onChange} className={styles.select} placeholder={placeholder}>
      {children}
    </select>
  );
