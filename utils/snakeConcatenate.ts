export const snakeConcatenate = (arrays: any[][]): any[] => {
  const result: any[] = [];

  for (let i = 0; i < arrays[0].length; i++) {
    for (let j = 0; j < arrays.length; j++) {
      result.push(arrays[j][i]);
    }
    if (i < arrays[0].length - 1) {
      i++;
      for (let j = arrays.length - 1; j >= 0; j--) {
        result.push(arrays[j][i]);
      }
    }
  }

  return result;
}